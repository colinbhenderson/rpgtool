﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestSpawn : MonoBehaviour
{
    public GameObject testPrefabSquare;
    public GameObject testPrefabHex;
    public GameObject parent;
    public GameObject SpawnPos;
    public int width = 0, height = 0;


    // Start is called before the first frame update
    void Start()
    { 
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void InstantiatePrefabForHex()
    {
        //Instantiate(TestPrefab,parent.transform.position + new Vector3(offsetX, 0, 0), Quaternion, parent.transform);
        //offsetX += TestPrefab.GetComponent<Renderer>().bounds.size.x;
        //InstantiateSquareGrid(30,30,SpawnPos.transform, testPrefab);
        InstantiateHexGrid(width, height, SpawnPos.transform, testPrefabHex);
    }

    public void InstantiatePrefabForSquare()
    {
        InstantiateSquareGrid(height,width,SpawnPos.transform, testPrefabSquare);
    }

    public void InstantiateSquareGrid(int width, int height, Transform spawnPoint, GameObject prefab)
    {
        Vector3 StartPoint = spawnPoint.position;
        GameObject temp;
        TileList tileList = new TileList();
        float offsetX = prefab.GetComponent<Renderer>().bounds.size.x;
        float offsetZ = prefab.GetComponent<Renderer>().bounds.size.z;

        for (int i = 0; i < height; i++)
        {
            for (int j = 0; j < width; j++)
            {
                temp = Instantiate(prefab, spawnPoint.position, new Quaternion(0,0,0,0), parent.transform);
                spawnPoint.position += new Vector3(offsetX, 0, 0);
                tileList.addTile(temp.GetComponent<SquareTile>());
            }
            spawnPoint.position = StartPoint;
            spawnPoint.position += new Vector3(0, 0, offsetZ);
            StartPoint = spawnPoint.position;
        }
        Resources.UnloadUnusedAssets();
    }

    public void InstantiateHexGrid(int width, int height, Transform spawnPoint, GameObject prefab)
    {
        Vector3 StartPoint = spawnPoint.position;
        Quaternion rotation = Quaternion.Euler(0, 30, 0);
        GameObject temp;
        TileList tileList = new TileList();
        float offsetX = prefab.GetComponent<Renderer>().bounds.size.x * 0.866f;
        float offsetZ = prefab.GetComponent<Renderer>().bounds.size.z * 0.866f;

        for (int i = 0; i < height; i++)
        {

            if (isOdd(i))
            {
                print("Called isodd");
                spawnPoint.position = StartPoint;
                spawnPoint.position += new Vector3(offsetX / 2, 0, offsetZ);
                StartPoint = spawnPoint.position;
            }
            else if(i != 0)
            {
                print("called even");
                spawnPoint.position = StartPoint;
                spawnPoint.position += new Vector3(-(offsetX / 2), 0, offsetZ);
                StartPoint = spawnPoint.position;
            }

            for (int j = 0; j < width; j++)
            {
                temp = Instantiate(prefab, spawnPoint.position, rotation, parent.transform);
                spawnPoint.position += new Vector3(offsetX, 0, 0);
                tileList.addTile(temp.GetComponent<HexTile>());
            }
        }

        Resources.UnloadUnusedAssets();
    }


    // ADDTO UTILS SCRIPT LATER
    public bool isOdd(int num)
    {
        if(num%2 == 0)
        {
            return false;
        }else
        {
            return true;
        }
    }
}
