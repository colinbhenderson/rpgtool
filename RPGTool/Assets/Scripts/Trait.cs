﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trait : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    public string traitName;
    public Dictionary<string, string> TraitData = new Dictionary<string, string>();
    //The first string is a Data Type
    //The second is the Value

    //Contructors
    public Trait()
    {

    }
/*
    public Trait(string newName, string dataType, string newData)
    {
        name = newName;
        TraitData.Add(dataType, newData);
    }*/

    public Trait(string newName, Dictionary<string, string> dataDict)
    {
        traitName = newName;
        AddTraitData(dataDict);
    }



    // Update is called once per frame
    void Update()
    {
        
    }


    //Gets the requested trait of the correct type
    public void AddTraitData(string dataType, string newData)
    {
        TraitData.Add(dataType, newData);
    }

 

    public void AddTraitData(Dictionary<string, string> dataDict)
    {
        foreach(var tmp in dataDict)
        {
            TraitData.Add(tmp.Key, tmp.Value);
        }
    }

    public void RemoveTraitData(string key)
    {
        TraitData.Remove(key);
    }

    public Dictionary<string, string> GetTraitData()
    {
        return TraitData;
    }




}
