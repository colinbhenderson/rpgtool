﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreateTraitButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public TraitManager traitmanager;
    public GameObject content;
    public Text text;
    public GameObject nameText;
    Dictionary<string, string> tempTraitDict = new Dictionary<string, string>();


    // Update is called once per frame
    void Update()
    {
        
    }

    public void MakeTrait()
    {
        int i = 1;
        string tempKey = "";
        string tempValue = "";
        foreach (Transform child in content.transform)
        {
            //Get the text boxes
            GameObject temp = child.gameObject; //Get the input box
            text = temp.transform.GetChild(2).GetComponent<Text>(); //Get the correct text field
            string tempText = text.text; //Get the string value of the user input
            Debug.Log(tempText);//Test

            //Check for if we are on the Key or Value
            if (i == 2)
            {
                tempValue = tempText;
                Debug.Log(tempKey + ": " + tempValue);
                tempTraitDict.Add(tempKey, tempValue);
                tempKey = "";
                tempValue = "";
                i = 1;

            }
            else if (i == 1)
            {
                tempKey = tempText;
                i += 1;
            }


        }

        string tempName = nameText.transform.GetChild(2).GetComponent<Text>().text;
        traitmanager.NewTrait(tempName, tempTraitDict);
        clear();
    }

    public void clear()
    {
        for (int i = 0; i < content.transform.childCount; i++)
        {
            nameText.GetComponent<InputField>().text = " ";
            Destroy(content.transform.GetChild(i).gameObject);
            
        }
    }


}
