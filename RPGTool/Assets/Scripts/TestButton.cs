﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestButton : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public TraitManager traitManager;

    // Update is called once per frame
    void Update()
    {
        
    }


    public void test()
    {

        Dictionary<string, Trait> tempDict = traitManager.GetTraitDict();

        foreach(var i in tempDict)
        {
            string traitName = i.Value.traitName;
            Debug.Log("Key: " + i.Key + " Trait Name: " + traitName);
        }
    }

}
