﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainContainer : MonoBehaviour
{
    public List<Material> terrainMaterials = new List<Material>();

    // Start is called before the first frame update
    void Start()
    {
        LoadMaterialsFromDisk();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public TerrainContainer()
    {

    }

    public void AddMaterialInstance(Material material)
    {
        terrainMaterials.Add(material);
    }

    public void AddMaterialInstance(List<Material> materials)
    {
        terrainMaterials.AddRange(materials);
    }

    public void LoadMaterialsFromDisk()
    {
        Debug.Log("Called");
        Material[] mats;
        mats = Resources.LoadAll<Material>("Material");
        List<Material> temp = new List<Material>(mats);
        AddMaterialInstance(temp);
    }


}
