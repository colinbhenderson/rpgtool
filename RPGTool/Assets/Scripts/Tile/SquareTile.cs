﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public class TileList
{
    List<SquareTile> tiles = new List<SquareTile>();

    public void addTile(SquareTile tile)
    {
        tiles.Add(tile);
    }

    public void addTileList(List<SquareTile> tileList)
    {
        tiles.AddRange(tileList);
    }
}

public class SquareTile : MonoBehaviour
{
    public int id = 0;
    public TerrainContainer terrain = new TerrainContainer();
    public bool isOccupied;
    public bool isVisible;
    // Start is called before the first frame update
    void Start()
    {
        GenerateTileValues();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GenerateTileValues()
    {
        id = Random.Range(0, 2000);
        terrain.LoadMaterialsFromDisk();
        GetComponent<Renderer>().material = terrain.terrainMaterials[Random.Range(0, 3)];
        isOccupied = false;
        isVisible = false;
    }
}
