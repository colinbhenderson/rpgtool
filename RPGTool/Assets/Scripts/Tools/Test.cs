﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public string RemoveCharsFromString(string str, char[] charArray)
    {
        List<char> chars = new List<char>(str);
        string empty = str;
        foreach(char c in chars)
        {
            int count = 0;
            foreach(char g in charArray)
            {
                if(c == g)
                {
                    count++;
                }
            }

            if(count == 0)
            {
                empty += c;
            }
            count = 0;

        }
        return empty;
    }

    public int StringToInt32(string str)
    {
        int temp = Int32.Parse(str);
        return temp;
    }
}
