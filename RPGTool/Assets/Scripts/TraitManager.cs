﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraitManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public Dictionary<string, Trait> traitsDictonary = new Dictionary<string, Trait>();
    public Trait trait;
    



    // Update is called once per frame
    void Update()
    {
        
    }


/*    public void NewTrait(string name, string dataType, string newData) 
    {
        Trait newTrait = new Trait(name, dataType, newData);
        traitsDictonary.Add(name, newTrait);
    }*/

    public void NewTrait(string name, Dictionary<string, string> dataDict)
    {
        Trait newTrait = new Trait(name, dataDict);
        traitsDictonary.Add(name, newTrait);
    }

    public Trait GetTrait(string traitName)
    {
        Trait value = new Trait();
        if(traitsDictonary.TryGetValue(traitName, out value))
        {
            return value;
        }
        else
        {
            Debug.LogError("Error: Trait Does not Exist");
            return null;
        }
    }


    public Dictionary<string, Trait> GetTraitDict()
    {
        return traitsDictonary;
    }


       







}
