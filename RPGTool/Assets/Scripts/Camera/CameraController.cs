﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraController : MonoBehaviour
{
    public GameObject tileContainer;
    public float renderDistance;
    public float sensitivity = 10f;
    public bool occlude;

    public List<GameObject> tiles = new List<GameObject>();
    private float moveSpeed = 0.5f;
    private float scrollSpeed = 10f;
    private Quaternion quat = new Quaternion(0,0,0,0);

    void Start()
    {
        occlude = false;
    }


    void Update()
    {
        if (Input.GetAxisRaw("Horizontal") != 0 || Input.GetAxisRaw("Vertical") != 0)
        {
            //transform.position += moveSpeed * new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
            transform.position += transform.right * Input.GetAxisRaw("Horizontal") * moveSpeed;
            transform.position += transform.up * Input.GetAxisRaw("Vertical") * moveSpeed;
            transform.position += transform.forward * Input.GetAxisRaw("Vertical") * moveSpeed;
        }

        if (Input.GetAxis("Mouse ScrollWheel") != 0)
        {
            transform.position += transform.forward * Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
        }

        if(Input.GetMouseButton(1))
        {
            var c = Camera.main.transform;
            quat.eulerAngles = new Vector3(0, Input.GetAxis("Mouse X") * sensitivity, 0);
            c.Rotate(quat.eulerAngles, Space.World);
        }

        if (occlude == true)
        {
            CalculateChildDistance();
        }
    }

    public void GetTileReference()
    {
        foreach (Transform child in tileContainer.transform)
        {
            tiles.Add(child.gameObject);
        }
    }

    private void CalculateChildDistance()
    {

        foreach(GameObject tile in tiles)
        {
            float dist = Vector3.Distance(tile.transform.position, transform.position);
            if(dist > renderDistance)
            {
                tile.SetActive(false);
            }
            else
            {
                tile.SetActive(true);
            }    
        }
    }
}
